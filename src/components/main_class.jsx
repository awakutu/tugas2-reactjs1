import React, { Component } from "react";

class Main extends Component {
  render() {
    return (
      <>
        <form className="form-group">
          <input
            type="text"
            id={this.props.nameInput}
            className="form-control w-25 mx-auto "
            placeholder={this.props.nameInput}
            aria-describedby={this.props.nameInput}
          />
        </form>
      </>
    );
  }
}

export default Main;
