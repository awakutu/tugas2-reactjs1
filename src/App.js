import React from 'react';
import Header from './components/header_function';
import Main from './components/main_class'

const App =() => {
  return (
    <>
    <Header name="React"/>
    <Main nameInput="username" />
    <Main nameInput="password" />
    </>
    
  );
}

export default App;
